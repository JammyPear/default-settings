# Default Settings -- FoundryVTT Module
Sets default settings to other players in Foundry. Useful to set a unified playing experience among your players.

## Installation
Use the install menu in the Module section of the Foundry Client

## Usage
Setting default settings for Players